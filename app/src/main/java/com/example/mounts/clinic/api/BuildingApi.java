package com.example.mounts.clinic.api;

import com.example.mounts.clinic.response.BuildingListResponse;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

public interface BuildingApi {

    @FormUrlEncoded
    @POST("api/building_list")
    Call<BuildingListResponse> getBuildingList(@Field("token") String token, @Field("type_id") int typeId, @Field("town_id") int townId);
}

package com.example.mounts.clinic.response;

import com.example.mounts.clinic.model.DoctorDetail;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DoctorDetailResponse {

    @SerializedName("is_success")
    public Boolean isSuccess;

    @SerializedName("doctor")
    public List<DoctorDetail> doctorDetail;
}

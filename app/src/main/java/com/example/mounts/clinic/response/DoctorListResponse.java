package com.example.mounts.clinic.response;

import com.example.mounts.clinic.model.Doctor;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DoctorListResponse {

    @SerializedName("is_success")
    public boolean isSuccess;

    @SerializedName("doctors")
    public List<Doctor> doctorLists;
}

package com.example.mounts.clinic.view;

import com.example.mounts.clinic.model.Doctor;

import java.util.List;

public interface DoctorView {

    public void doctorView(List<Doctor> doctorList);
}

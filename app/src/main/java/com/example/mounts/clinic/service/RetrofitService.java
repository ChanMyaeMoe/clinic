package com.example.mounts.clinic.service;

import com.example.mounts.clinic.baseurl.BaseUrl;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class RetrofitService {
    public  Retrofit getService(){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl.BASED_URL)
                .addConverterFactory( GsonConverterFactory.create())
                .build();
        return  retrofit;
    }
}

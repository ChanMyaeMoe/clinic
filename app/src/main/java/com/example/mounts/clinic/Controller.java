package com.example.mounts.clinic;

import android.util.Log;

import com.example.mounts.clinic.api.SpecializationListApi;
import com.example.mounts.clinic.model.SpecializationList;
import com.example.mounts.clinic.response.SpecializationListResponse;
import com.example.mounts.clinic.service.RetrofitService;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class Controller {

    String token;
    RetrofitService service;
    List<SpecializationList> specializationLists = new ArrayList<>();

    public Controller(String token) {
        this.token = token;
        service = new RetrofitService();

    }

    public List<SpecializationList> getSpecialist() {
        SpecializationListApi specializationListApi = service.getService().create(SpecializationListApi.class);

        specializationListApi.getSpecializationList(token).enqueue(new Callback<SpecializationListResponse>() {
            @Override
            public void onResponse(Response<SpecializationListResponse> response, Retrofit retrofit) {

                if (response.isSuccess()) {
                    if (response.body().isSuccess) {

                        specializationLists = response.body().specializations;
                        Log.e("Special size", String.valueOf(response.body().specializations.size()));

//
//                        for (SpecializationList special : specializationLists) {
//                            categories.add(special.name);
//                            Log.e("name",special.name);
//                        }
//                        Log.e("cate size",String.valueOf(categories.size()));

                    }
                }

            }

            @Override
            public void onFailure(Throwable t) {

            }
        });

        return specializationLists;
    }

}

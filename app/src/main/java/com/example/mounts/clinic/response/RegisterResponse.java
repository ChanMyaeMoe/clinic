package com.example.mounts.clinic.response;

import com.google.gson.annotations.SerializedName;

public class RegisterResponse {
    @SerializedName("token")
    public String token;

    @SerializedName("is_success")
    public Boolean isSuccess;
}



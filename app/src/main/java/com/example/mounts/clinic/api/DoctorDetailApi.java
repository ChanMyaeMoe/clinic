package com.example.mounts.clinic.api;

import com.example.mounts.clinic.response.DoctorDetailResponse;
import com.example.mounts.clinic.response.DoctorListResponse;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

public interface DoctorDetailApi {


    @FormUrlEncoded
    @POST("api/doctor_detail")
    Call<DoctorDetailResponse> getDoctorDetail(@Field("token") String token, @Field("doctor_id") int id);
}

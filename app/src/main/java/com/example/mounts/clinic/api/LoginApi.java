package com.example.mounts.clinic.api;

import com.example.mounts.clinic.response.LoginResponse;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

public interface LoginApi {
    @FormUrlEncoded
    @POST("api/login")
    Call<LoginResponse> login(@Field("phoneNumber") long phone, @Field("password") String password);
}

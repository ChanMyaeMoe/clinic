package com.example.mounts.clinic.model;

import com.google.gson.annotations.SerializedName;

public class Building {

    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;

    @SerializedName("town_name")
    public String townName;

    @SerializedName("type_id")
    public int typeId;

    @SerializedName("type_name")
    public String typeName;

    public Building(){

    }
}

package com.example.mounts.clinic.activity;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.mounts.clinic.R;
import com.example.mounts.clinic.api.RegisterApi;
import com.example.mounts.clinic.response.RegisterResponse;
import com.example.mounts.clinic.service.RetrofitService;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText edtphone, edtpass1, edtpass2;
    private Button btnRegister;
    private ProgressBar progressBar;

    private RetrofitService service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        init();
    }

    private void init() {
        service = new RetrofitService();

        edtphone = findViewById(R.id.edtphone);
        edtpass1 = findViewById(R.id.edtpass1);
        edtpass2 = findViewById(R.id.edtpass2);
        progressBar=findViewById(R.id.progressBar);
        btnRegister = findViewById(R.id.btnSubmit);
        btnRegister.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        long phone = Long.parseLong(edtphone.getText().toString());
        String pass1 = edtpass1.getText().toString();
        String pass2 = edtpass2.getText().toString();

        Log.e("Phone num", String.valueOf(phone));
        Log.e("pass", pass1);
        Log.e("pass num", pass2);

        if (pass1.length() < 6) {

            edtpass1.setError("minimum 6 characters");
        } else if (pass2.length() < 6) {
            edtpass2.setError("minimum 6 characters");
        } else if (pass1.equals(pass2)) {

            progressBar.setVisibility(View.VISIBLE);

            userRegister(phone, pass1, pass2);

            Log.e("ONclick", "success");
        } else {

            edtpass1.setError("Incorrect password");
            edtpass2.setError("Incorrect password");

        }
    }

    private void userRegister(long phone, String pass1, String pass2) {

        RegisterApi registerApi = service.getService().create(RegisterApi.class);

        registerApi.register(phone,pass1,pass2).enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Response<RegisterResponse> response, Retrofit retrofit) {
                if(response.isSuccess()){
                    Toast.makeText(RegisterActivity.this, "Register Success", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                    startActivity(intent);
                    progressBar.setVisibility(View.GONE);
                    finish();
                    Log.e("RegisterOnResponse", "success");
                }

                else {
                    Log.e("RegisterOnResponse", "fail");
                    Toast.makeText(RegisterActivity.this, "Register fail", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });

    }
}

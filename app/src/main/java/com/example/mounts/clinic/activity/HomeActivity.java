package com.example.mounts.clinic.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.mounts.clinic.R;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnHome, btnArticle, btnDoctor, btnHospital, btnLab, btnClinc;
    private Intent intent;
    private String token;
    private Bundle b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        initHomeActivity();
    }

    private void initHomeActivity() {

        setContentView(R.layout.activity_home);
        btnHome = findViewById(R.id.btn_home);
        btnArticle = findViewById(R.id.btn_articles);
        btnDoctor = findViewById(R.id.btn_doctor);
        btnHospital = findViewById(R.id.btn_hospital);
        btnLab = findViewById(R.id.btn_lab);
        btnClinc = findViewById(R.id.btn_clinic);

        btnHome.setOnClickListener(this);
        btnArticle.setOnClickListener(this);
        btnDoctor.setOnClickListener(this);
        btnHospital.setOnClickListener(this);
        btnLab.setOnClickListener(this);
        btnClinc.setOnClickListener(this);

        b = getIntent().getExtras();

        token = b.getString("Token");
        Log.e("HomeActivityToken", token);

    }

    @Override
    public void onClick(View v) {

        if (v == btnArticle) {
            Toast.makeText(HomeActivity.this, "This is Articles", Toast.LENGTH_LONG).show();

        }

        if (v == btnHome) {
            Toast.makeText(HomeActivity.this, "This is Home", Toast.LENGTH_LONG).show();
        }
        if (v == btnDoctor) {


            intent = new Intent(HomeActivity.this, DrawerActivity.class);

            startNextActivity(intent);

        }
        if (v == btnHospital) {


            intent = new Intent(HomeActivity.this, HospitalActivity.class);
            startNextActivity(intent);

        }
        if (v == btnLab) {
            intent = new Intent(HomeActivity.this, LabActivity.class);
            startNextActivity(intent);

        }
        if (v == btnClinc) {


            intent = new Intent(HomeActivity.this, ClinicActivity.class);
            startNextActivity(intent);

        }

    }

    private void startNextActivity(Intent intent) {

        intent.putExtra("Token", token);
        startActivity(intent);
    }

}

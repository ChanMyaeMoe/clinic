package com.example.mounts.clinic.api;

import com.example.mounts.clinic.response.RegisterResponse;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

public interface RegisterApi {

//    @FormUrlEncoded
//    @POST("api/register")
//    Call<RegisterResponse> register(@Field("phoneNumber") long phone, @Field("password") String pass1, @Field("password_confirmation") String pass2);

    @FormUrlEncoded
    @POST("api/register")
    Call<RegisterResponse> register(@Field("phoneNumber") long phone, @Field("password") String password, @Field("password_confirmation") String password2);

}


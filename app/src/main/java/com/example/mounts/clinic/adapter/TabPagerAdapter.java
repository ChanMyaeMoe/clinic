package com.example.mounts.clinic.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.mounts.clinic.fragment.ArticleFragment;
import com.example.mounts.clinic.fragment.HomeFragment;

public class TabPagerAdapter extends FragmentStatePagerAdapter {

    String[] tab = {"Home", "Articles"};

    public TabPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {

        switch (i) {
            case 0:
                HomeFragment fragment = new HomeFragment();
                return fragment;

            case 1:
                ArticleFragment fragment1 = new ArticleFragment();
                return fragment1;


        }

        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }


    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return tab[position];
    }
}

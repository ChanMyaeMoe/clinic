package com.example.mounts.clinic.activity;

import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.mounts.clinic.R;
import com.example.mounts.clinic.adapter.DoctorAdapter;
import com.example.mounts.clinic.api.DoctorListApi;
import com.example.mounts.clinic.holder.DoctorHolder;
import com.example.mounts.clinic.model.Doctor;
import com.example.mounts.clinic.response.DoctorListResponse;
import com.example.mounts.clinic.service.RetrofitService;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class DoctorListActivity extends AppCompatActivity implements DoctorHolder.OnDoctorClickListener {

    private SearchView searchView;
    private RecyclerView recyclerView;
    private RetrofitService service;
    private Spinner spinner;
    DoctorAdapter adapter;

    List<Doctor> doctors = new ArrayList<>();
    List<Doctor> newDoctors = new ArrayList<>();
    private String token ;
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initDoctorList();


        searchViewModify();
        searchViewFilter();
        getDoctorsList();

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


    }


    //initial work of activity
    private void initDoctorList() {

        setContentView(R.layout.activity_main);
        searchView = findViewById(R.id.sv);
        recyclerView = findViewById(R.id.recyclerView);
        service = new RetrofitService();
        adapter = new DoctorAdapter(this);
        spinner = findViewById(R.id.spinner);

        // Spinner click listener
//        spinner.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Dentist");
        categories.add("OG");
        categories.add("Endocrinologists");
        categories.add("Cardiologist");
        categories.add("Addiction");
        categories.add("Simple");
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
    }

    private void getDoctorsList() {

        DoctorListApi doctorListApi = service.getService().create(DoctorListApi.class);
        doctorListApi.getDoctorList(token).enqueue(new Callback<DoctorListResponse>() {
            @Override
            public void onResponse(Response<DoctorListResponse> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    if (response.body().isSuccess) {
                        doctors = response.body().doctorLists;
                        adapter.addDoctors(doctors);

                        Log.e("DoctorLists", String.valueOf(doctors.size()));
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    //search view filter

    private void searchViewFilter() {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {

                s = s.toLowerCase(Locale.getDefault());
                if (s.length() != 0) {
                    newDoctors.clear();
                    for (Doctor doctor : doctors) {
                        if (doctor.getName().toLowerCase(Locale.getDefault()).contains(s)) {

                            newDoctors.add(doctor);
                        }
                    }
                    adapter.addDoctors(newDoctors);
                } else {
                    adapter.addDoctors(doctors);
                }
                Toast.makeText(DoctorListActivity.this, s, Toast.LENGTH_LONG).show();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {

                s = s.toLowerCase(Locale.getDefault());
                if (s.length() != 0) {
                    newDoctors.clear();
                    for (Doctor doctor : doctors) {
                        if (doctor.getName().toLowerCase(Locale.getDefault()).contains(s)) {

                            newDoctors.add(doctor);
                        }
                    }
                    adapter.addDoctors(newDoctors);
                } else {
                    adapter.addDoctors(doctors);
                }

                Toast.makeText(DoctorListActivity.this, s, Toast.LENGTH_LONG).show();
                return false;
            }
        });
    }

    //search view modify

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void searchViewModify() {
        searchView.setIconified(false);
        searchView.setIconifiedByDefault(false);
        SearchView.SearchAutoComplete searchAutoComplete = searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchAutoComplete.setHint("Search Doctors");
        searchAutoComplete.setHintTextColor(Color.WHITE);
        searchAutoComplete.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        searchAutoComplete.setTextSize(21);

//        /*Code for changing the search icon */
//        ImageView searchIcon = searchView.findViewById(android.support.v7.appcompat.R.id.search_mag_icon);
//
//        searchIcon.setImageResource(R.drawable.ic_search);
//
//        ImageView searchViewIcon = searchView.findViewById(android.support.v7.appcompat.R.id.search_mag_icon);
//
//        ViewGroup linearLayoutSearchView =
//                (ViewGroup) searchViewIcon.getParent();
//        linearLayoutSearchView.removeView(searchViewIcon);
//        linearLayoutSearchView.addView(searchViewIcon);
//        searchViewIcon.setAdjustViewBounds(true);
//        searchViewIcon.setMaxWidth(0);
//        searchViewIcon.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));


    }

    @Override
    public void onDoctorClick(int id) {

    }
}

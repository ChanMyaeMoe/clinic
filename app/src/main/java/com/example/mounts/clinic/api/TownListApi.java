package com.example.mounts.clinic.api;

import com.example.mounts.clinic.response.TownListResponse;
import com.google.gson.annotations.SerializedName;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

public interface TownListApi {

    @FormUrlEncoded
    @POST("api/town_list")
    Call<TownListResponse> getTownList(@Field("token") String token);
}

package com.example.mounts.clinic.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.mounts.clinic.R;
import com.example.mounts.clinic.activity.ClinicActivity;
import com.example.mounts.clinic.activity.DrawerActivity;
import com.example.mounts.clinic.activity.HomeActivity;
import com.example.mounts.clinic.activity.HospitalActivity;
import com.example.mounts.clinic.activity.LabActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements View.OnClickListener {

    private Button btnHome, btnArticle, btnDoctor, btnHospital, btnLab, btnClinc;
    private Intent intent;
    private String token;
    private Bundle b;


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);


        btnHome = view.findViewById(R.id.btn_home);
        btnArticle = view.findViewById(R.id.btn_articles);
        btnDoctor = view.findViewById(R.id.btn_doctor);
        btnHospital = view.findViewById(R.id.btn_hospital);
        btnLab = view.findViewById(R.id.btn_lab);
        btnClinc = view.findViewById(R.id.btn_clinic);

//        btnHome.setOnClickListener(this);
//        btnArticle.setOnClickListener(this);
        btnDoctor.setOnClickListener(this);
        btnHospital.setOnClickListener(this);
        btnLab.setOnClickListener(this);
        btnClinc.setOnClickListener(this);

        b = getActivity().getIntent().getExtras();
//        savedInstanceState.getString("Token");

        token = b.getString("Token");
        Log.e("HomeActivityToken", token);
        return view;

    }

    @Override
    public void onClick(View v) {


//        if (v == btnArticle) {
//            Toast.makeText(getActivity(), "This is Articles", Toast.LENGTH_LONG).show();
//
//        }

//        if (v == btnHome) {
//            Toast.makeText(getActivity(), "This is Home", Toast.LENGTH_LONG).show();
//        }
        if (v == btnDoctor) {


            intent = new Intent(getActivity(), DrawerActivity.class);

            startNextActivity(intent);

        }
        if (v == btnHospital) {


            intent = new Intent(getActivity(), HospitalActivity.class);
            startNextActivity(intent);

        }
        if (v == btnLab) {
            intent = new Intent(getActivity(), LabActivity.class);
            startNextActivity(intent);

        }
        if (v == btnClinc) {


            intent = new Intent(getActivity(), ClinicActivity.class);
            startNextActivity(intent);

        }

    }

    private void startNextActivity(Intent intent) {

        intent.putExtra("Token", token);
        startActivity(intent);
    }
}

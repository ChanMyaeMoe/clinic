package com.example.mounts.clinic.api;

import com.example.mounts.clinic.response.DoctorListResponse;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

public interface DoctorListApi {

    @FormUrlEncoded
    @POST("api/doctor_list")
    Call<DoctorListResponse> getDoctorList(@Field("token") String token);
}

package com.example.mounts.clinic.holder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mounts.clinic.R;
import com.example.mounts.clinic.model.Building;
import com.example.mounts.clinic.model.Doctor;
import com.example.mounts.clinic.response.ExpandAndCollapseViewUtil;
import com.squareup.picasso.Picasso;


public class BuildingHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

   private TextView txName,txType,txLocation,txId;

    private ViewGroup linearLayoutDetails,linearLayoutMore,layoutDetail;
    private ImageView imageViewExpand;

    private static final int DURATION = 250;
    private OnBuildingClickListener listener;


    public interface OnBuildingClickListener {
        public void onBuildingClick(int id);//to carry doctorName from one class to another
    }

    public BuildingHolder(@NonNull View itemView, OnBuildingClickListener listener) {
        super(itemView);
        this.listener=listener;
        initView(itemView);
        itemView.setOnClickListener(this);
    }

    private void initView(View view) {
        txId=view.findViewById(R.id.txid);
        txName=view.findViewById(R.id.txName);
        txType=view.findViewById(R.id.txType);
        txLocation=view.findViewById(R.id.txLocation);
        imageViewExpand=view.findViewById(R.id.imageViewExpand);
        linearLayoutDetails=view.findViewById(R.id.linearLayoutDetails);
        linearLayoutMore=view.findViewById(R.id.layout_more);
        layoutDetail=view.findViewById(R.id.layout_detail);
        linearLayoutMore.setOnClickListener(this);
        layoutDetail.setOnClickListener(this);

    }

    public void bindData(Building building) {
        txName.setText(building.name);
        txType.setText(building.typeName);
        txLocation.setText(building.townName);
        txId.setText(String.valueOf(building.id));
    }



    public void toggleDetails(View view) {
        if (linearLayoutDetails.getVisibility() == View.GONE) {
            ExpandAndCollapseViewUtil.expand(linearLayoutDetails, DURATION);
            imageViewExpand.setImageResource(R.drawable.ic_more);
            rotate(-180.0f);
        } else {
            ExpandAndCollapseViewUtil.collapse(linearLayoutDetails, DURATION);
            imageViewExpand.setImageResource(R.drawable.ic_less);
            rotate(180.0f);
        }
    }

    private void rotate(float angle) {
        Animation animation = new RotateAnimation(0.0f, angle, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setFillAfter(true);
        animation.setDuration(DURATION);
        imageViewExpand.startAnimation(animation);
    }

    @Override
    public void onClick(View v) {
        listener.onBuildingClick(Integer.parseInt(txId.getText().toString()));
        int position;
        position = getAdapterPosition();
        Log.e("position",String.valueOf(position));
//        toggleDetails(v);
    }


    public static BuildingHolder create(LayoutInflater inflater, ViewGroup parent, OnBuildingClickListener listener) {
        View view = inflater.inflate(R.layout.building_item, parent, false);
        return new BuildingHolder(view, listener);
    }

}

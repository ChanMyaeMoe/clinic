package com.example.mounts.clinic.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DoctorDetail {

    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;

    @SerializedName("town_name")
    public String townName;

    @SerializedName("photo")
    public String photo;

    @SerializedName("specialists")
    public String specialists;

    @SerializedName("clinics")
    List<String> clinics;
}

package com.example.mounts.clinic.api;

import com.example.mounts.clinic.response.DoctorListResponse;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

public interface DoctorsBySpecialApi {

    @FormUrlEncoded
    @POST("api/doctor_list_by_specialization")
    Call<DoctorListResponse> getDoctorList(@Field("token") String token , @Field("specialization_id") int id);
}

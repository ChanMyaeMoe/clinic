package com.example.mounts.clinic.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Gravity;
import android.view.PointerIcon;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.mounts.clinic.R;
import com.example.mounts.clinic.adapter.DoctorAdapter;
import com.example.mounts.clinic.api.DoctorListApi;
import com.example.mounts.clinic.api.DoctorsBySpecialApi;
import com.example.mounts.clinic.api.SpecializationListApi;
import com.example.mounts.clinic.holder.DoctorHolder;
import com.example.mounts.clinic.model.Doctor;
import com.example.mounts.clinic.model.SpecializationList;
import com.example.mounts.clinic.response.DoctorListResponse;
import com.example.mounts.clinic.response.SpecializationListResponse;
import com.example.mounts.clinic.service.RetrofitService;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class DrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, DoctorHolder.OnDoctorClickListener, Spinner.OnItemSelectedListener {

    private Intent intent;
    private SearchView searchView;
    private RecyclerView recyclerView;
    private RetrofitService service;
    //    private MaterialBetterSpinner spinner;
    private Spinner spinner;
    DoctorAdapter adapter;
    ArrayAdapter<String> dataAdapter;
    List<SpecializationList> specializationLists = new ArrayList<>();
    List<String> categories = new ArrayList<>();

    List<Doctor> doctors = new ArrayList<>();
    List<Doctor> newDoctors = new ArrayList<>();
    private String token = null;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

//        getSpecializationList(token);
        initDoctorList();

        searchViewFilter();
        getDoctorsList();
//        searchViewModify();


//         Drop down layout style - list view with radio button


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//
//        getMenuInflater().inflate(R.menu.search_view_menu, menu);
//
//        MenuItem searchItem = menu.findItem(R.id.action_search);
//        SearchView searchView =
//                (SearchView) MenuItemCompat.getActionView(searchItem);
//
//        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
//
//        searchView.setSearchableInfo(
//                searchManager.getSearchableInfo(getComponentName()));
//
//        return super.onCreateOptionsMenu(menu);
//
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        } else if (id == R.id.nav_logout) {

            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);// 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            editor.clear();
            editor.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    //initial work of activity
    private void initDoctorList() {


        searchView = findViewById(R.id.sv);
        recyclerView = findViewById(R.id.recyclerView);
        service = new RetrofitService();
        adapter = new DoctorAdapter(this);
        spinner = findViewById(R.id.spinner);
        Bundle b = getIntent().getExtras();
        token = b.getString("Token");


        Log.e("DrawerActivityToken", token);


        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


//         Spinner click listener
        spinner.setOnItemSelectedListener(this);
        // Spinner Drop down elements
//        List<String> location = new ArrayList<>();
//        location.add("Location");
//        location.add("Mandalay");
//        location.add("Yandon");
//        location.add("Taunggyi");
//        location.add("Naypyitaw");

        dataAdapter = new ArrayAdapter<String>(getBaseContext(), R.layout.spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinner.setTextColor(Color.WHITE);
//        spinner.setHighlightColor(Color.WHITE);
        spinner.setAdapter(dataAdapter);
        getSpecializationList(token);


        // attaching data adapter to spinner

        // Creating adapter for spinner

    }

    private void getSpecializationList(String token) {


        SpecializationListApi specializationListApi = service.getService().create(SpecializationListApi.class);

        specializationListApi.getSpecializationList(token).enqueue(new Callback<SpecializationListResponse>() {
            @Override
            public void onResponse(Response<SpecializationListResponse> response, Retrofit retrofit) {

                if (response.isSuccess()) {
                    if (response.body().isSuccess) {

                        specializationLists = response.body().specializations;
                        Log.e("Special size", String.valueOf(response.body().specializations.size()));


                        for (SpecializationList special : specializationLists) {
                            categories.add(special.name);
                            Log.e("name", special.name);
                        }
                        categories.add(0, "All");
                        dataAdapter.notifyDataSetChanged();
                        Toast.makeText(getApplicationContext(), "Successful!", Toast.LENGTH_SHORT).show();


                    }
                }

            }

            @Override
            public void onFailure(Throwable t) {

            }

        });


    }

    private void getDoctorsList() {

        DoctorListApi doctorListApi = service.getService().create(DoctorListApi.class);
        doctorListApi.getDoctorList(token).enqueue(new Callback<DoctorListResponse>() {
            @Override
            public void onResponse(Response<DoctorListResponse> response, Retrofit retrofit) {

                if (response.isSuccess()) {
                    if (response.body().isSuccess) {
                        doctors = response.body().doctorLists;
                        adapter.addDoctors(doctors);

                        Log.e("DoctorLists", String.valueOf(doctors.size()));
                    }
                }

            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    //search view filter

    private void searchViewFilter() {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {

                s = s.toLowerCase(Locale.getDefault());
                if (s.length() != 0) {
                    newDoctors.clear();
                    for (Doctor doctor : doctors) {
                        if (doctor.getName().toLowerCase(Locale.getDefault()).contains(s)) {

                            newDoctors.add(doctor);
                        }
                    }
                    adapter.addDoctors(newDoctors);
                } else {
                    adapter.addDoctors(doctors);
                }
                Toast.makeText(DrawerActivity.this, s, Toast.LENGTH_LONG).show();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {

                s = s.toLowerCase(Locale.getDefault());
                if (s.length() != 0) {
                    newDoctors.clear();
                    for (Doctor doctor : doctors) {
                        if (doctor.getName().toLowerCase(Locale.getDefault()).contains(s)) {

                            newDoctors.add(doctor);
                        }
                    }
                    adapter.addDoctors(newDoctors);
                } else {
                    adapter.addDoctors(doctors);
                }

                Toast.makeText(DrawerActivity.this, s, Toast.LENGTH_LONG).show();
                return false;
            }
        });
    }

    //search view modify

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void searchViewModify() {
        searchView.setIconified(false);
        searchView.setIconifiedByDefault(false);
        SearchView.SearchAutoComplete searchAutoComplete = searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchAutoComplete.setHint("Search Doctors");
        searchAutoComplete.setHintTextColor(Color.WHITE);
        searchAutoComplete.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);

        ImageView searchIcon = searchView.findViewById(android.support.v7.appcompat.R.id.search_mag_icon);
        searchIcon.focusSearch(View.FOCUS_RIGHT);


    }


    @Override
    public void onDoctorClick(int id) {

        Intent intent = new Intent(this, DoctorDetailActivity.class);
        intent.putExtra("id", id);
        Log.e("doctor_id", String.valueOf(id));
        startActivity(intent);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        String name = spinner.getSelectedItem().toString();
        if (name.equals("All")) {
            adapter.addDoctors(doctors);
        } else {

            int special_id = 0;
            Log.e("spinner selected item", name);
            for (SpecializationList specializationList : specializationLists) {
                if (specializationList.name.equals(name)) {

                    special_id = specializationList.id;
                    Log.e("special_id", String.valueOf(id));


                }
            }
            DoctorsBySpecialApi doctorsBySpecialApi = service.getService().create(DoctorsBySpecialApi.class);
            doctorsBySpecialApi.getDoctorList(token, special_id).enqueue(new Callback<DoctorListResponse>() {
                @Override
                public void onResponse(Response<DoctorListResponse> response, Retrofit retrofit) {

                    if (response.isSuccess()) {
                        if (response.body().isSuccess) {
                            adapter.addDoctors(response.body().doctorLists);

                            Log.e("DoctorLists", String.valueOf(doctors.size()));
                        }
                    }

                }

                @Override
                public void onFailure(Throwable t) {

                }
            });

        }


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}

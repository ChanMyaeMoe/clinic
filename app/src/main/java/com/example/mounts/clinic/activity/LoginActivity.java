package com.example.mounts.clinic.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.mounts.clinic.R;
import com.example.mounts.clinic.api.LoginApi;
import com.example.mounts.clinic.response.LoginResponse;
import com.example.mounts.clinic.service.RetrofitService;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnLogin, btnRegister;
    private EditText edtphone, edtpass1;
    private RetrofitService service;
    private String token = null;
    private ProgressBar progressBar;
    SharedPreferences pref;// 0 - for private mode
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        editor = pref.edit();
        initLogin();
    }

    private void initLogin() {

        service = new RetrofitService();

        edtphone = findViewById(R.id.ph_num_id);
        edtpass1 = findViewById(R.id.password_id);
        btnLogin = findViewById(R.id.btn_login);
        btnRegister = findViewById(R.id.btn_register);
        token = pref.getString("token", null);
        progressBar=findViewById(R.id.progressBar);

        if (token != null) {

            Intent intent = new Intent(getApplicationContext(), HomeNaviActivity.class);
            intent.putExtra("Token", token);
            startActivity(intent);
            finish();
        } else {

            btnLogin.setOnClickListener(this);
            btnRegister.setOnClickListener(this);
        }

    }

    @Override
    public void onClick(View v) {
        if (v == btnLogin) {
            progressBar.setVisibility(View.VISIBLE);
            long phone = Long.parseLong(edtphone.getText().toString());
            String pass1 = edtpass1.getText().toString();

            Log.e("phone_num", String.valueOf(phone));

            Log.e("pass", pass1);

//            Intent intent=new Intent(LoginActivity.this,HomeActivity.class);
//            intent.putExtra("Token","This is token id.");
//            startActivity(intent);
            userLogin(phone, pass1);

        } else if (v == btnRegister) {

            userRegister();
        }
    }

    private void userRegister() {

        Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
        startActivity(intent);
    }

    private void userLogin(long phone, String pass1) {
        LoginApi loginApi = service.getService().create(LoginApi.class);


        loginApi.login(phone, pass1).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Response<LoginResponse> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    Log.e("Token", response.body().token);

                    token = response.body().token;

                    editor.putString("token", token);
                    editor.apply();
                    editor.commit();

//                    SaveSharedPreference.setLoggedIn(getApplicationContext(), true);
//                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    Intent intent = new Intent(getApplicationContext(), HomeNaviActivity.class);
                    intent.putExtra("Token", response.body().token);
                    Toast.makeText(LoginActivity.this, "Login Success", Toast.LENGTH_LONG).show();
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    progressBar.setVisibility(View.GONE);
                    finish();

                } else {

                    Toast.makeText(LoginActivity.this, "Login Fail", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {

                Log.e("Throwable", t.toString());

            }
        });
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}


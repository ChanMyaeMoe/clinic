package com.example.mounts.clinic.api;

import com.example.mounts.clinic.response.SpecializationListResponse;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

public interface SpecializationListApi {

    @FormUrlEncoded
    @POST("api/specialization_list")
    Call<SpecializationListResponse> getSpecializationList(@Field("token") String token);
}

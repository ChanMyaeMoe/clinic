package com.example.mounts.clinic.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.mounts.clinic.R;
import com.example.mounts.clinic.adapter.BuildingAdapter;
import com.example.mounts.clinic.adapter.DoctorAdapter;
import com.example.mounts.clinic.api.BuildingApi;
import com.example.mounts.clinic.api.DoctorListApi;
import com.example.mounts.clinic.api.TownListApi;
import com.example.mounts.clinic.holder.BuildingHolder;
import com.example.mounts.clinic.model.Building;
import com.example.mounts.clinic.model.Doctor;
import com.example.mounts.clinic.model.TownList;
import com.example.mounts.clinic.response.BuildingListResponse;
import com.example.mounts.clinic.response.DoctorListResponse;
import com.example.mounts.clinic.response.TownListResponse;
import com.example.mounts.clinic.service.RetrofitService;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class ClinicActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, BuildingHolder.OnBuildingClickListener,Spinner.OnItemSelectedListener {

    private Intent intent;
    private SearchView searchView;
    private RecyclerView recyclerView;
    private RetrofitService service;
    //    private MaterialBetterSpinner spinnerType,spinnerLocation;
    private Spinner spinnerType, spinnerLocation;
    BuildingAdapter adapter;

    List<Building> building = new ArrayList<>();
    List<Building> newBuildings = new ArrayList<>();
    List<String> location = new ArrayList<>();
    List<String> type = new ArrayList<>();
    private String token = null;
    private int typeId = 1;
    private int townId = 0;
    ArrayAdapter<String> dataAdapter;
    private List<TownList> townLists=new ArrayList<>();

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clinic);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        initBuildingList();


//        searchViewModify();
        searchViewFilter();
        getBuildingList(typeId, townId);

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


    }

    private void getLocationList(String token) {

        final TownListApi townListApi=service.getService().create(TownListApi.class);

        townListApi.getTownList(token).enqueue(new Callback<TownListResponse>() {
            @Override
            public void onResponse(Response<TownListResponse> response, Retrofit retrofit) {
                if(response.isSuccess()){
                    if(response.body().isSuccess){
                        townLists.addAll(response.body().towns);
                        for(TownList townList:townLists){
                            location.add(townList.name);
                            Log.e("locations",townList.name);
                        }
                        adapter.notifyDataSetChanged();
                        Toast.makeText(getApplicationContext(), "Successful!", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//
//        getMenuInflater().inflate(R.menu.search_view_menu, menu);
//
//        MenuItem searchItem = menu.findItem(R.id.action_search);
//        SearchView searchView =
//                (SearchView) MenuItemCompat.getActionView(searchItem);
//
//        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
//
//        searchView.setSearchableInfo(
//                searchManager.getSearchableInfo(getComponentName()));
//
//        return super.onCreateOptionsMenu(menu);
//
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        } else if (id == R.id.nav_logout) {

            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);// 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            editor.clear();
            editor.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    //initial work of activity
    private void initBuildingList() {


        searchView = findViewById(R.id.sv);
        recyclerView = findViewById(R.id.recyclerView);
        service = new RetrofitService();
        adapter = new BuildingAdapter(this);
        spinnerType = findViewById(R.id.spinner_type);
        spinnerLocation = findViewById(R.id.spinner_location);
        Bundle b = getIntent().getExtras();
        token = b.getString("Token");

        Log.e("ClinicActivityToken", token);


        // Spinner click listener
//        spinner.setOnItemSelectedListener(this);

        // Spinner Drop down elements

        type.add("Clinic");
        type.add("Hospital");
        type.add("Lab");
        type.add("All");
        // Creating adapter for spinner

        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(getBaseContext(), R.layout.spinner_item, type);

        // Drop down layout style - list view with radio button

        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

//        spinnerType.setTextColor(Color.WHITE);
//        spinnerType.setHighlightColor(Color.WHITE);
//        spinnerLocation.setTextColor(Color.WHITE);


        // attaching data adapter to spinner
        spinnerType.setAdapter(dataAdapter1);
        spinnerType.setOnItemSelectedListener(this);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getBaseContext(), R.layout.spinner_item, location);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        location.add(0,"All");
        spinnerLocation.setAdapter(dataAdapter);
        spinnerLocation.setOnItemSelectedListener(this);
        getLocationList(token);
    }

    private void getBuildingList(int typeId, int townId) {

        BuildingApi buildingApi = service.getService().create(BuildingApi.class);

        buildingApi.getBuildingList(token, typeId, townId).enqueue(new Callback<BuildingListResponse>() {
            @Override
            public void onResponse(Response<BuildingListResponse> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    if (response.body().isSuccess) {
                        building = response.body().buildingList;
                        adapter.addItem(building);
                        Log.e("Clinic_building_size", String.valueOf(building.size()));
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });

    }


    private void searchViewFilter() {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {

                s = s.toLowerCase(Locale.getDefault());
                if (s.length() != 0) {
                    newBuildings.clear();
                    for (Building building : building) {
                        if (building.name.toLowerCase(Locale.getDefault()).contains(s)) {

                            newBuildings.add(building);
                        }
                    }
                    adapter.addItem(newBuildings);
                } else {
                    adapter.addItem(building);
                }
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {

                s = s.toLowerCase(Locale.getDefault());
                if (s.length() != 0) {
                    newBuildings.clear();
                    for (Building building : building) {
                        if (building.name.toLowerCase(Locale.getDefault()).contains(s)) {

                            newBuildings.add(building);
                        }
                    }
                    adapter.addItem(newBuildings);
                } else {
                    adapter.addItem(building);
                }

                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
                return false;
            }
        });
    }

    //search view modify

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void searchViewModify() {
        searchView.setIconified(false);
        searchView.setIconifiedByDefault(false);
        SearchView.SearchAutoComplete searchAutoComplete = searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchAutoComplete.setHint("Search Doctors");
        searchAutoComplete.setHintTextColor(Color.WHITE);
        searchAutoComplete.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);


////        searchView.setLayoutParams(new ActionBar.LayoutParams(Gravity.RIGHT));

//        searchView.setLayoutParams(new ActionBar.LayoutParams(Gravity.RIGHT));
//        searchAutoComplete.setTextSize(21);

//        /*Code for changing the search icon */
        ImageView searchIcon = searchView.findViewById(android.support.v7.appcompat.R.id.search_mag_icon);
        searchIcon.focusSearch(View.FOCUS_RIGHT);

//
//        searchIcon.setRight(30);

//        searchIcon.setImageResource(R.drawable.ic_search);

//        ImageView searchViewIcon = searchView.findViewById(android.support.v7.appcompat.R.id.search_mag_icon);
//
//        ViewGroup linearLayoutSearchView =
//                (ViewGroup) searchView.getParent();
//        linearLayoutSearchView.removeView(searchView);
//        linearLayoutSearchView.addView(searchView);
//        searchView.setMaxWidth(0);
//        searchView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));


    }

    @Override
    public void onBuildingClick(int id) {
        Intent intent = new Intent(this, DoctorDetailActivity.class);
        intent.putExtra("id", id);
        Log.e("doctor_id", String.valueOf(id));
        startActivity(intent);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        String typeName = spinnerType.getSelectedItem().toString();
        String locationName = spinnerLocation.getSelectedItem().toString();
        Log.e("typeName",typeName);

        if (locationName.equals("All")){
            townId=0;
        }

        for (TownList townList : townLists) {
            if (townList.name.equals(locationName)) {
                townId = townList.id;
                int e = Log.e(" name:locationId", townList.name + String.valueOf(townId));
                break;
            }
        }

        switch (typeName) {
            case "Clinic":

                typeId = 1;
                break;

            case "Hospital":
                typeId = 2;
                break;

            case "Lab":
                typeId = 3;
                break;

            case "All":
                typeId = 0;
                break;
        }

        getBuildingList(typeId, townId);

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
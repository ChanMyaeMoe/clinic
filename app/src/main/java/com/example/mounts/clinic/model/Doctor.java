package com.example.mounts.clinic.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Doctor {

    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;

    @SerializedName("towns")
    public List<String > towns;

    @SerializedName("photo")
    public String photo;

    @SerializedName("specialists")
    public List<String> specialists;

//    private String specialized,imageUrl;
//    private long phone;
//    private int profile;

//
//    public String getImageUrl() {
//        return imageUrl;
//    }
//
//    public void setImageUrl(String imageUrl) {
//        this.imageUrl = imageUrl;
//    }

    public Doctor(){
    }

//    public Doctor(String name){
//        this.name=name;
//    }
//    public Doctor(String name, String specialized, String address, long phone, int imageUrl) {
//        this.name = name;
//        this.specialized = specialized;
//        this.phone = phone;
//        this.profile=imageUrl;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getSpecialized() {
        return specialists;
    }

    public void setSpecialized(List<String> specialists) {
        this.specialists = specialists;
    }


}

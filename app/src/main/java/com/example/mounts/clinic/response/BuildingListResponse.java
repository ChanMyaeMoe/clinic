package com.example.mounts.clinic.response;

import com.example.mounts.clinic.model.Building;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class BuildingListResponse {

    @SerializedName("isSuccess")
    public boolean isSuccess;

    @SerializedName("next_page_url")
    public String nextPage;

    @SerializedName("previous_page_url")
    public String previousPage;

    @SerializedName("first_page_url")
    public String firstPage;

    @SerializedName("last_page_url")
    public String lastPage;

    @SerializedName("buildings")
    public ArrayList<Building> buildingList;
}

package com.example.mounts.clinic.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.mounts.clinic.activity.DrawerActivity;
import com.example.mounts.clinic.holder.DoctorHolder;
import com.example.mounts.clinic.model.Doctor;

import java.util.ArrayList;
import java.util.List;

public class DoctorAdapter extends RecyclerView.Adapter<DoctorHolder> {


    List<Doctor> doctorLists;
    private DoctorHolder.OnDoctorClickListener listener;

    public DoctorAdapter(DoctorHolder.OnDoctorClickListener listener){
        doctorLists =new ArrayList<>();
        this.listener=listener;
    }

    @NonNull
    @Override
    public DoctorHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater inflater=LayoutInflater.from(viewGroup.getContext());
        return DoctorHolder.create(inflater,viewGroup,listener);
    }

    @Override
    public void onBindViewHolder(@NonNull DoctorHolder doctorHolder, int i) {

        doctorHolder.bindData(doctorLists.get(i));

    }

    @Override
    public int getItemCount() {
        return doctorLists.size();
    }


    public void addDoctors(List<Doctor> doctorList) {
        this.doctorLists.clear();
        this.doctorLists.addAll(doctorList);
        notifyDataSetChanged();

    }
//
//    public void filter(String charText) {
//        List<Doctor> newDoctorList =new ArrayList<>();
//        charText = charText.toLowerCase(Locale.getDefault());
//         if(charText.length()!=0) {
//             newDoctorList.clear();
//            for (Doctor doctor : this.doctorLists) {
//                if (doctor.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
//
//                   newDoctorList.add(doctor);
//                }
//            }
//            addDoctors(newDoctorList);
//        }
//    }

}
